# <img src="https://upload.wikimedia.org/wikipedia/en/thumb/c/ce/Spirent_logo.svg/490px-Spirent_logo.svg.png" alt="Spirent Logo" width="200"> <br/> Spirent VisionWorks On-Premises Environment ChangeLog

### 2.0.0 (2021-11-24)

Second major update of the on-prem environment where all infrastructure services are moved out into a separate Helm chart

#### Features
* Kubernetes single-instance cluster

#### Breaking Changes
All infrastructure services were moved to a package-infraservices-helm component where they are installed in dockerized form by a helm chart

### 1.0.0 (2021-08-30)

Initial release of the on-premises environment

#### Features
* Create, update and delete scripts
* Kubernetes cluster
* Timescale database
* Kafka message broker running in Kubernetes
* Redis distributed cache running in Kubernetes

#### Breaking Changes
No breaking changes since this is the first version

#### Bug Fixes
No fixes in this version

