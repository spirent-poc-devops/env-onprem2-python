# -*- coding: utf-8 -*-
"""
    component.delete
    ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    
    Function to delete a component
    
    :copyright: Spirent Commynications 2021, see AUTHORS for more details.
    :license: ISC, see LICENSE for more details.
"""
import os
from common import read_config, read_resources, write_resources
from common import get_map_value, remove_map_value, test_map_value
from common import write_info
from common import build_template
from common import run_process

def delete(config_path, resources_path):
    # Read config and resources
    config = read_config(config_path)
    resources = read_resources(resources_path)

    path = os.path.abspath(os.path.dirname(__file__))
    if not os.path.exists(path + "/../temp/k8s_ansible_hosts"):
        # Create ansible hosts file
        ansible_inventory = "[master_init]\n"
        if (len(get_map_value(config, "k8s.master_ips")) > 1):
            # Initial master
            ansible_inventory += f"master_init ansible_host={k8s_master_ips[0]} ansible_ssh_user={k8s_instance_username} ansible_ssh_private_key_file={k8s_instance_key_path}\n"

            # Redundant masters
            ansible_inventory += "[masters_redundant]\n"
            i = 0
            for k8s_master_ip in k8s_master_ips[1:]:
                ansible_inventory += f"master_redundant{i} ansible_host={k8s_master_ip} ansible_ssh_user={k8s_instance_username} ansible_ssh_private_key_file={k8s_instance_key_path}\n"
                i += 1

            # Loadbalancer
            if get_map_value(config, "k8s.loadbalancer.haproxy"):
                ansible_inventory += "[lb]\n"
                lb_host = get_map_value(config, "k8s.loadbalancer.host")
                ansible_inventory += f"lb0 ansible_host={k8s_master_ips[0]} ansible_ssh_user={k8s_instance_username} ansible_ssh_private_key_file={k8s_instance_key_path}\n"
        else:
            # Single master
            ansible_inventory += f"master_init ansible_host={k8s_master_ips} ansible_ssh_user={k8s_instance_username} ansible_ssh_private_key_file={k8s_instance_key_path}\n"

        # Workers
        ansible_inventory += "[workers]\n"
        i = 0
        for k8s_worker_ip in k8s_worker_ips:
            ansible_inventory += f"worker{i} ansible_host={k8s_worker_ip} ansible_ssh_user={k8s_instance_username} ansible_ssh_private_key_file={k8s_instance_key_path}\n"
            i += 1
            
        # Save ansible hosts file
        with open(path + "/../temp/k8s_ansible_hosts", 'w') as f:
            f.write(ansible_inventory)
    
    # Delete k8s cluster
    if get_map_value(config, "extended_ansible_logs"):
        run_process("ansible-playbook -v -i " + path + "/../temp/k8s_ansible_hosts " + path + "/templates/k8s_uninstall_playbook.yml")
    else:
        run_process("ansible-playbook -i " + path + "/../temp/k8s_ansible_hosts " + path + "/templates/k8s_uninstall_playbook.yml")

    # Remove k8s cluster from mgmt station kubeconfig
    env_prefix = get_map_value(config, "environment.prefix")
    k8s_username = get_map_value(config, "k8s.username")
    mgmt_kube_config_path = "/home/" + k8s_username + "/.kube/config"

    if os.path.exists(mgmt_kube_config_path):
        write_info("kubernetes", "Removing " + env_prefix +" k8s cluster from mgmt station .kube/config")
        run_process("kubectl config unset users." + env_prefix + "-k8s-admin")
        run_process("kubectl config unset contexts." + env_prefix + "-k8s-admin")
        run_process("kubectl config unset clusters." + env_prefix + "-k8s-admin")
        
        # If last k8s cluster deleted need to cleanup kubeconfig
        with open(mgmt_kube_config_path, 'r') as file :
            filedata = file.read()

        filedata = filedata.replace('null', '')

        with open(mgmt_kube_config_path, 'w') as file:
            file.write(filedata)

        # Delete env config
        if os.path.exists(mgmt_kube_config_path + "_" + env_prefix):
            os.remove(mgmt_kube_config_path + "_" + env_prefix)

    # Remove resource values
    if test_map_value(resources, "k8s"):
        remove_map_value(resources, "k8s.type")
        remove_map_value(resources, "k8s.inventory")
        write_resources(resources_path, resources)
