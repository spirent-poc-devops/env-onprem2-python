# -*- coding: utf-8 -*-
"""
    component.create
    ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    
    Function to create a component
    
    :copyright: Spirent Commynications 2021, see AUTHORS for more details.
    :license: ISC, see LICENSE for more details.
"""

import os
from common import read_config, read_resources, write_resources
from common import get_map_value, set_map_value
from common import build_template
from common import write_error
from common import run_process, read_process

def create(config_path, resources_path):
    # Read config and resources
    config = read_config(config_path)
    resources = read_resources(resources_path)

    path = os.path.abspath(os.path.dirname(__file__))

    k8s_master_ips = get_map_value(config, "k8s.master_ips")
    k8s_worker_ips = get_map_value(config, "k8s.worker_ips")
    k8s_instance_username = get_map_value(config, "k8s.username")
    k8s_instance_key_path = get_map_value(config, "k8s.private_key_path")

    # Create ansible hosts file
    ansible_inventory = "[master_init]\n"
    ansible_inventory += f"master_init ansible_host={k8s_master_ips[0]} ansible_ssh_user={k8s_instance_username} ansible_ssh_private_key_file={k8s_instance_key_path}\n"

    if (len(get_map_value(config, "k8s.master_ips")) > 1):        
        # Redundant masters
        ansible_inventory += "[masters_redundant]\n"
        i = 0
        for k8s_master_ip in k8s_master_ips[1:]:
            ansible_inventory += f"master_redundant{i} ansible_host={k8s_master_ip} ansible_ssh_user={k8s_instance_username} ansible_ssh_private_key_file={k8s_instance_key_path}\n"
            i += 1

        # Loadbalancer
        if get_map_value(config, "k8s.loadbalancer.haproxy"):
            ansible_inventory += "[lb]\n"
            lb_host = get_map_value(config, "k8s.loadbalancer.host")
            ansible_inventory += f"lb0 ansible_host={k8s_master_ips[0]} ansible_ssh_user={k8s_instance_username} ansible_ssh_private_key_file={k8s_instance_key_path}\n"

    # Workers
    ansible_inventory += "[workers]\n"
    i = 0
    for k8s_worker_ip in k8s_worker_ips:
        ansible_inventory += f"worker{i} ansible_host={k8s_worker_ip} ansible_ssh_user={k8s_instance_username} ansible_ssh_private_key_file={k8s_instance_key_path}\n"
        i += 1
        
    # Save ansible hosts file
    with open(path + "/../temp/k8s_ansible_hosts", 'w') as f:
        f.write(ansible_inventory)

    # Whitelist k8s vm
    if get_map_value(config, "extended_ansible_logs"):
        run_process("ansible-playbook -v -i " + path + "/../temp/k8s_ansible_hosts " + path + "/templates/ssh_keyscan_playbook.yml")
    else:
        run_process("ansible-playbook -i " + path + "/../temp/k8s_ansible_hosts " + path + "/templates/ssh_keyscan_playbook.yml")

    # Install k8s dependencies
    template_params = {
        "k8s_kubelet_version": get_map_value(config, "k8s.kubelet_version"),
        "k8s_kubeadm_version": get_map_value(config, "k8s.kubeadm_version"),
        "k8s_kubectl_version": get_map_value(config, "k8s.kubectl_version")
    }
    
    build_template(
        path + "/templates/k8s_prerequisites_playbook.yml",
        path + "/../temp/k8s_prerequisites_playbook.yml",
        template_params
    )

    if get_map_value(config, "extended_ansible_logs"):
        run_process("ansible-playbook -v -i " + path + "/../temp/k8s_ansible_hosts " + path + "/../temp/k8s_prerequisites_playbook.yml")
    else:
        run_process("ansible-playbook -i " + path + "/../temp/k8s_ansible_hosts " + path + "/../temp/k8s_prerequisites_playbook.yml")

    if (len(get_map_value(config, "k8s.master_ips")) > 1):
        # Multimaster cluster

        # Configure loadbalancer
        if get_map_value(config, "k8s.loadbalancer.haproxy"):
            haproxy_masters_endpoints = ""

            for k8s_master_ip in k8s_master_ips:
                haproxy_masters_endpoints += f"                server master1 {k8s_master_ip}:6443 check\n"
            
            template_params = {
                "haproxy_masters_endpoints": haproxy_masters_endpoints
            }

            build_template(
                path + "/templates/k8s_lb_playbook.yml",
                path + "/../temp/k8s_lb_playbook.yml",
                template_params
            )

            if get_map_value(config, "extended_ansible_logs"):
                run_process("ansible-playbook -v -i " + path + "/../temp/k8s_ansible_hosts " + path + "/../temp/k8s_lb_playbook.yml")
            else:
                run_process("ansible-playbook -i " + path + "/../temp/k8s_ansible_hosts " + path + "/../temp/k8s_lb_playbook.yml")

        # Init k8s cluster on master node
        template_params = {
            "env_prefix": get_map_value(config, "environment.prefix"),
            "k8s_network": get_map_value(config, "k8s.network"),
            "k8s_username": get_map_value(config, "k8s.username"),
            "lb_host": get_map_value(config, "k8s.loadbalancer.host"),
            "lb_port": get_map_value(config, "k8s.loadbalancer.port")
        }

        build_template(
            path + "/templates/k8s_master_init_playbook.yml",
            path + "/../temp/k8s_master_init_playbook.yml",
            template_params
        )

        if get_map_value(config, "extended_ansible_logs"):
            run_process("ansible-playbook -v -i " + path + "/../temp/k8s_ansible_hosts " + path + "/../temp/k8s_master_init_playbook.yml")
        else:
            run_process("ansible-playbook -i " + path + "/../temp/k8s_ansible_hosts " + path + "/../temp/k8s_master_init_playbook.yml")

        # Init redundant masters
        template_params = {
            "k8s_network": get_map_value(config, "k8s.network"),
            "k8s_username": get_map_value(config, "k8s.username")
        }

        build_template(
            path + "/templates/k8s_master_redundant_playbook.yml",
            path + "/../temp/k8s_master_redundant_playbook.yml",
            template_params
        )

        if get_map_value(config, "extended_ansible_logs"):
            run_process("ansible-playbook -v -i " + path + "/../temp/k8s_ansible_hosts " + path + "/../temp/k8s_master_redundant_playbook.yml")
        else:
            run_process("ansible-playbook -i " + path + "/../temp/k8s_ansible_hosts " + path + "/../temp/k8s_master_redundant_playbook.yml")

    else:
        # Single master cluster
        template_params = {
            "env_prefix": get_map_value(config, "environment.prefix"),
            "k8s_network": get_map_value(config, "k8s.network"),
            "k8s_username": get_map_value(config, "k8s.username")
        }

        build_template(
            path + "/templates/k8s_master_single_playbook.yml",
            path + "/../temp/k8s_master_single_playbook.yml",
            template_params
        )

        if get_map_value(config, "extended_ansible_logs"):
            run_process("ansible-playbook -v -i " + path + "/../temp/k8s_ansible_hosts " + path + "/../temp/k8s_master_single_playbook.yml")
        else:
            run_process("ansible-playbook -i " + path + "/../temp/k8s_ansible_hosts " + path + "/../temp/k8s_master_single_playbook.yml")

    # Join workers to master
    if get_map_value(config, "extended_ansible_logs"):
        run_process("ansible-playbook -v -i " + path + "/../temp/k8s_ansible_hosts " + path + "/templates/k8s_worker_playbook.yml")
    else:
        run_process("ansible-playbook -i " + path + "/../temp/k8s_ansible_hosts " + path + "/templates/k8s_worker_playbook.yml")

    # Add env prefix to cluster name
    env_prefix = get_map_value(config, "environment.prefix")
    k8s_username = get_map_value(config, "k8s.username")
    kube_config_path = "/home/" + k8s_username + "/.kube/config_" + env_prefix

    with open(kube_config_path, 'r') as file :
        kube_config_data = file.read()

    kube_config_data = kube_config_data.replace('kubernetes-admin@kubernetes', env_prefix + "-k8s")
    kube_config_data = kube_config_data.replace('kubernetes', env_prefix + "-k8s")

    with open(kube_config_path, 'w') as file:
        file.write(kube_config_data)

    # Merge new kube config to mgmt station kube config
    if not os.path.exists("/home/" + k8s_username + "/.kube/config") or \
        os.stat("/home/" + k8s_username + "/.kube/config").st_size == 0:
        # mgmt kubeconfig not exists or empty
        with open("/home/" + k8s_username + "/.kube/config", 'w') as file:
            file.write(kube_config_data)
    else:
        clusters = read_process("kubectl config get-contexts -o name")
        if env_prefix + "-k8s" not in clusters:
            with open(kube_config_path, 'r') as file :
                new_kube_config = file.readlines()

            new_cluster_section = "".join(new_kube_config[2:5])
            new_context_section = "".join(new_kube_config[7:10])
            new_user_section = "".join(new_kube_config[15:18])

            with open("/home/" + k8s_username + "/.kube/config", 'r') as file :
                mgmt_kube_config = file.readlines()

            new_mgmt_kube_config = []

            for line in mgmt_kube_config:
                new_mgmt_kube_config.append(line)
                if "clusters:" in line:
                    new_mgmt_kube_config.append(new_cluster_section)
                elif "contexts:" in line:
                    new_mgmt_kube_config.append(new_context_section)
                elif "users:" in line:
                    new_mgmt_kube_config.append(new_user_section)

            with open("/home/" + k8s_username + "/.kube/config", 'w') as file:
                file.write("".join(new_mgmt_kube_config))

    # Save resources
    set_map_value(resources, "k8s.type", "kubeadm")
    set_map_value(resources, "k8s.inventory", ansible_inventory)
    write_resources(resources_path, resources)
    