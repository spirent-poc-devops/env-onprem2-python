# -*- coding: utf-8 -*-
"""
    component.terraform
    ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    
    Function to process terraform scripts
    
    :copyright: Spirent Commynications 2021, see AUTHORS for more details.
    :license: ISC, see LICENSE for more details.
"""

import os
import glob
import shutil
from common import sync_terraform_state
from common import get_map_value, set_map_value
from common import build_template

def env_keypair_terraform(config, resources):
    vars_str = "-var aws_cred_file=" + get_map_value(config, "hw.aws.credentials_file") + \
        " -var aws_region=" + get_map_value(config, "hw.aws.region") + \
        " -var aws_profile=" + get_map_value(config, "hw.aws.profile") + \
        " -var aws_stage=" + get_map_value(config, "hw.aws.stage") + \
        " -var aws_namespace=" + get_map_value(config, "hw.aws.namespace") + \
        " -var env_nameprefix=" + get_map_value(config, "environment.prefix")
    
    
    return vars_str

def init_keypair_terraform(environment_prefix):
    path = os.path.abspath(os.path.dirname(__file__))
    keypair_terraform_path = path + "/../temp/hw_keypair_" + environment_prefix

    # Create terraform folder if not exists
    if not os.path.exists(keypair_terraform_path):
        os.mkdir(keypair_terraform_path)

    # Load terraform state
    sync_terraform_state("hw_keypair", environment_prefix)

    # Copy terraform scripts to terraform folder
    shutil.copy(path + "/templates/provider.tf", keypair_terraform_path)
    shutil.copy(path + "/templates/keypair.tf", keypair_terraform_path)

    return keypair_terraform_path

def env_k8s_terraform(config, resources):
    vars_str = "-var aws_cred_file=" + get_map_value(config, "hw.aws.credentials_file") + \
        " -var aws_region=" + get_map_value(config, "hw.aws.region") + \
        " -var aws_profile=" + get_map_value(config, "hw.aws.profile") + \
        " -var aws_vpc=" + get_map_value(config, "hw.aws.vpc") + \
        " -var k8s_instance_ami=" + get_map_value(config, "hw.k8s.instance_ami") + \
        " -var k8s_instance_type=" + get_map_value(config, "hw.k8s.instance_type") + \
        " -var k8s_keypair_name=" + get_map_value(resources, "hw.k8s.keypair_name") + \
        " -var k8s_subnet_id=" + get_map_value(config, "hw.k8s.subnet_id") + \
        " -var k8s_subnet_cidr=" + get_map_value(config, "hw.k8s.subnet_cidr") + \
        " -var k8s_volume_size=" + str(get_map_value(config, "hw.k8s.volume_size")) + \
        " -var k8s_associate_public_ip_address=" + get_map_value(config, "hw.k8s.associate_public_ip_address") + \
        " -var k8s_ebs_snapshot_id=" + get_map_value(config, "hw.k8s.ebs_snapshot_id") + \
        " -var mgmt_subnet_cidr=" + get_map_value(config, "hw.mgmt_station.subnet_cidr") + \
        " -var env_nameprefix=" + get_map_value(config, "environment.prefix") # + \
        # " -var mgmt_public_ip=" + get_map_value(resources, "hw.mgmt_station.public_ip")
    
    return vars_str

def init_k8s_terraform(config, environment_prefix):
    path = os.path.abspath(os.path.dirname(__file__))
    k8s_terraform_path = path + "/../temp/k8s_" + environment_prefix

    # Create terraform folder if not exists
    if not os.path.exists(k8s_terraform_path):
        os.mkdir(k8s_terraform_path)

    # Load terraform state
    sync_terraform_state("k8s", environment_prefix)

    k8s_vm_count = int(get_map_value(config, "hw.k8s.nodes_count")) + int(get_map_value(config, "hw.k8s.masters_count"))
    for i in range(k8s_vm_count):
        template_params = { 
            "k8s_vm_index": str(i)
        }

        build_template(
            f"{path}/templates/k8s_vm.tf",
            f"{k8s_terraform_path}/k8s_vm_{i}.tf",
            template_params
        )

    # Copy terraform scripts to terraform folder
    shutil.copy(path + "/templates/provider.tf", k8s_terraform_path)
    shutil.copy(path + "/templates/k8s_variables.tf", k8s_terraform_path)

    return k8s_terraform_path

def env_k8s_lb_terraform(config, resources):
    vars_str = "-var aws_cred_file=" + get_map_value(config, "hw.aws.credentials_file") + \
        " -var aws_region=" + get_map_value(config, "hw.aws.region") + \
        " -var aws_profile=" + get_map_value(config, "hw.aws.profile") + \
        " -var aws_vpc=" + get_map_value(config, "hw.aws.vpc") + \
        " -var k8s_lb_instance_ami=" + get_map_value(config, "hw.k8s.instance_ami") + \
        " -var k8s_lb_instance_type=" + get_map_value(config, "hw.k8s.instance_type") + \
        " -var k8s_lb_keypair_name=" + get_map_value(resources, "hw.k8s.keypair_name") + \
        " -var k8s_lb_subnet_id=" + get_map_value(config, "hw.k8s.subnet_id") + \
        " -var k8s_subnet_cidr=" + get_map_value(config, "hw.k8s.subnet_cidr") + \
        " -var mgmt_subnet_cidr=" + get_map_value(config, "hw.mgmt_station.subnet_cidr") + \
        " -var env_nameprefix=" + get_map_value(config, "environment.prefix")

    return vars_str

def init_k8s_lb_terraform(environment_prefix):
    path = os.path.abspath(os.path.dirname(__file__))
    k8s_lb_terraform_path = path + "/../temp/k8s_lb_" + environment_prefix

    # Create terraform folder if not exists
    if not os.path.exists(k8s_lb_terraform_path):
        os.mkdir(k8s_lb_terraform_path)

    # Load terraform state
    sync_terraform_state("k8s_lb", environment_prefix)

    # Copy terraform scripts to terraform folder
    shutil.copy(path + "/templates/provider.tf", k8s_lb_terraform_path)
    shutil.copy(path + "/templates/k8s_lb.tf", k8s_lb_terraform_path)

    return k8s_lb_terraform_path


def env_mgmt_terraform(config, resources):
    vars_str = "-var env_prefix=" + get_map_value(config, "environment.prefix") + \
        " -var aws_region=" + get_map_value(config, "hw.aws.region") + \
        " -var aws_credentials_file=" + get_map_value(config, "hw.aws.credentials_file") + \
        " -var aws_profile=" + get_map_value(config, "hw.aws.profile") + \
        " -var vpc=" + get_map_value(config, "hw.aws.vpc") + \
        " -var instance_type=" + get_map_value(config, "hw.mgmt_station.instance_type") + \
        " -var ami=" + get_map_value(config, "hw.mgmt_station.instance_ami") + \
        " -var ami_owner=" + get_map_value(config, "hw.mgmt_station.ami_owner") + \
        " -var subnet=" + get_map_value(config, "hw.mgmt_station.subnet") + \
        " -var sg_id=" + get_map_value(config, "hw.mgmt_station.sg_id") + \
        " -var root_volume_size=" + str(get_map_value(config, "hw.mgmt_station.root_volume_size"))
    
    return vars_str


def init_mgmt_terraform(environment_prefix):
    path = os.path.abspath(os.path.dirname(__file__))
    mgmt_terraform_path = path + "/../temp/mgmt_station_" + environment_prefix

    # Create terraform folder if not exists
    if not os.path.exists(mgmt_terraform_path):
        os.mkdir(mgmt_terraform_path)

    # Load terraform state
    sync_terraform_state("mgmt_station", environment_prefix)

    # Copy terraform scripts to terraform folder
    for f in glob.glob(path + "/templates/mgmt_station/*.tf"):
        shutil.copy(f, mgmt_terraform_path)

    return mgmt_terraform_path