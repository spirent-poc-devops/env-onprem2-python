# -*- coding: utf-8 -*-
"""
    component.__init__
    ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    
    Component module definition
    
    :copyright: Spirent Commynications 2021, see AUTHORS for more details.
    :license: ISC, see LICENSE for more details.
"""

__all__ = [
    'env_keypair_terraform', 'init_keypair_terraform', 'env_k8s_terraform', 'init_k8s_terraform',
    'env_k8s_lb_terraform', 'init_k8s_lb_terraform', 'env_mgmt_terraform', 'init_mgmt_terraform',
    'create', 'update', 'delete'
]

from .terraform import env_keypair_terraform, init_keypair_terraform, env_k8s_terraform, init_k8s_terraform, env_k8s_lb_terraform, init_k8s_lb_terraform, env_mgmt_terraform, init_mgmt_terraform
from .create import create
from .update import update
from .delete import delete