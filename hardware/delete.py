# -*- coding: utf-8 -*-
"""
    component.delete
    ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    
    Function to delete a component
    
    :copyright: Spirent Commynications 2021, see AUTHORS for more details.
    :license: ISC, see LICENSE for more details.
"""

import os
from common import read_config, read_resources, write_resources
from common import get_map_value, remove_map_value, test_map_value
from common import remove_terraform_state
from common import write_error
from common import run_process
from hardware import env_keypair_terraform, init_keypair_terraform, env_k8s_terraform, init_k8s_terraform, env_k8s_lb_terraform, init_k8s_lb_terraform, env_mgmt_terraform, init_mgmt_terraform

def delete(config_path, resources_path):
    # Read config and resources
    config = read_config(config_path)
    resources = read_resources(resources_path)

    environment_prefix = get_map_value(config, "environment.prefix")
    path = os.path.abspath(os.path.dirname(__file__))


    # ### Kubernetes Virtual Machine ###


    # Set terraform env variables and init terraform folder
    tf_vars = env_k8s_terraform(config, resources)
    k8s_terraform_path = init_k8s_terraform(config, environment_prefix)

    # Run terraform scripts
    os.chdir(k8s_terraform_path)
    exit_code = run_process("terraform init")
    if exit_code != 0:
        os.chdir(path + "/..")
        write_error("mgmt_station", "Can't initialize terraform. Watch logs above or check " + k8s_terraform_path +" folder content.")
    
    exit_code = run_process("terraform plan " + tf_vars)
    if exit_code != 0:
        os.chdir(path + "/..")
        write_error("mgmt_station", "Can't execute terraform plan. Watch logs above or check " + k8s_terraform_path +" folder content.")
    
    exit_code = run_process("terraform destroy -auto-approve " + tf_vars)
    if exit_code != 0:
        os.chdir(path + "/..")
        write_error("mgmt_station", "Can't delete cloud resources. Watch logs above or check " + k8s_terraform_path +" folder content.")
    
    os.chdir(path + "/..")

    remove_terraform_state("k8s", environment_prefix)

    # Remove from resources
    k8s_vm_count = int(get_map_value(config, "hw.k8s.nodes_count")) + int(get_map_value(config, "hw.k8s.masters_count"))
    if test_map_value(resources, "hw.k8s"):
        for i in range(k8s_vm_count):
            remove_map_value(resources, f"hw.k8s.{i}_id")
            remove_map_value(resources, f"hw.k8s.{i}_private_ip")
            remove_map_value(resources, f"hw.k8s.{i}_public_ip")
            remove_map_value(resources, f"hw.k8s.{i}_sg_id")

    # Remove from config
    if test_map_value(config, "k8s"):
        remove_map_value(config, "k8s.username")
        remove_map_value(config, "k8s.private_key_path")
        remove_map_value(config, "k8s.master_ips")
        remove_map_value(config, "k8s.worker_ips")
        write_resources(config_path, config)


    # ### Kubernetes Load balancer virtual machine ###


    if get_map_value(config, "hw.k8s.masters_count") > 1:
        # Set terraform env variables and init terraform folder
        tf_vars = env_k8s_lb_terraform(config, resources)
        k8s_lb_terraform_path = init_k8s_lb_terraform(environment_prefix)

        # Run terraform scripts
        os.chdir(k8s_lb_terraform_path)
        exit_code = run_process("terraform init")
        if exit_code != 0:
            os.chdir(path + "/..")
            write_error("kubernetes_lb", "Can't initialize terraform. Watch logs above or check " + k8s_lb_terraform_path +" folder content.")
        
        exit_code = run_process("terraform plan " + tf_vars)
        if exit_code != 0:
            os.chdir(path + "/..")
            write_error("kubernetes_lb", "Can't execute terraform plan. Watch logs above or check " + k8s_lb_terraform_path +" folder content.")
        
        exit_code = run_process("terraform destroy -auto-approve " + tf_vars)
        if exit_code != 0:
            os.chdir(path + "/..")
            write_error("kubernetes_lb", "Can't delete cloud resources. Watch logs above or check " + k8s_lb_terraform_path +" folder content.")
        
        os.chdir(path + "/..")

        remove_terraform_state("k8s_lb", environment_prefix)

        if test_map_value(resources, "hw.k8s"):
            remove_map_value(resources, "hw.k8s.lb_id")
            remove_map_value(resources, "hw.k8s.lb_private_ip")
            remove_map_value(resources, "hw.k8s.lb_public_ip")
            remove_map_value(resources, "hw.k8s.lb_sg_id")
            write_resources(resources_path, resources)


    # ### Management station ###


    # Set terraform env variables and init terraform folder 
    tf_vars = env_mgmt_terraform(config, resources)
    mgmt_terraform_path = init_mgmt_terraform(environment_prefix)

    # Run terraform scripts
    os.chdir(mgmt_terraform_path)
    exit_code = run_process("terraform init")
    if exit_code != 0:
        os.chdir(path + "/..")
        write_error("mgmt_station", "Can't initialize terraform. Watch logs above or check " + mgmt_terraform_path +" folder content.")
    
    exit_code = run_process("terraform plan " + tf_vars)
    if exit_code != 0:
        os.chdir(path + "/..")
        write_error("mgmt_station", "Can't execute terraform plan. Watch logs above or check " + mgmt_terraform_path +" folder content.")
    
    exit_code = run_process("terraform destroy -auto-approve " + tf_vars)
    if exit_code != 0:
        os.chdir(path + "/..")
        write_error("mgmt_station", "Can't delete cloud resources. Watch logs above or check " + mgmt_terraform_path +" folder content.")
    
    os.chdir(path + "/..")

    remove_terraform_state("mgmt_station", environment_prefix)

    if test_map_value(resources, "hw.mgmt_station"):
        remove_map_value(resources, "hw.mgmt_station.key_name")
        remove_map_value(resources, "hw.mgmt_station.public_ip")
        remove_map_value(resources, "hw.mgmt_station.private_ip")
        remove_map_value(resources, "hw.mgmt_station.ssh_command")
        write_resources(resources_path, resources)


    # ### Keypair ###


    # Set terraform env variables and init terraform folder
    tf_vars = env_keypair_terraform(config, resources)
    keypair_terraform_path = init_keypair_terraform(environment_prefix)

    # Run terraform scripts
    os.chdir(keypair_terraform_path)
    exit_code = run_process("terraform init")
    if exit_code != 0:
        os.chdir(path + "/..")
        write_error("hw_keypair", "Can't initialize terraform. Watch logs above or check " + keypair_terraform_path +" folder content.")
    
    exit_code = run_process("terraform plan " + tf_vars)
    if exit_code != 0:
        os.chdir(path + "/..")
        write_error("hw_keypair", "Can't execute terraform plan. Watch logs above or check " + keypair_terraform_path +" folder content.")
    
    exit_code = run_process("terraform destroy -auto-approve " + tf_vars)
    if exit_code != 0:
        os.chdir(path + "/..")
        write_error("hw_keypair", "Can't delete cloud resources. Watch logs above or check " + keypair_terraform_path +" folder content.")
    
    os.chdir(path + "/..")

    remove_terraform_state("keypair_k8s", environment_prefix)

    if test_map_value(resources, "hw.k8s"):
        remove_map_value(resources, "hw.k8s.keypair_name")

    if test_map_value(resources, "hw.mgmt"):
        remove_map_value(resources, "hw.mgmt.keypair_name")

    write_resources(resources_path, resources)

