variable "k8s_instance_ami" {
  type        = string
  description = "Kubernetes vm instance ami"
}

variable "k8s_instance_type" {
  type        = string
  description = "Kubernetes vm instance type"
}

variable "k8s_keypair_name" {
  type        = string
  description = "Kubernetes vm instance keypair name"
}

variable "k8s_associate_public_ip_address" {
  type        = string
  description = "Define is kubernetes vm instance have public ip [true/false]"
}

variable "k8s_subnet_id" {
  type        = string
  description = "Kubernetes vm subnet id"
}

variable "k8s_volume_size" {
  type        = string
  description = "Kubernetes vm instance volume size"
}

variable "env_nameprefix" {
  type        = string
  description = "Environment prefix used for naming resources"
}

variable "aws_vpc" {
  type        = string
  description = "Kubernetes vm AWS vpc"
}

variable "mgmt_subnet_cidr" {
  type        = string
  description = "Management station CIDR"
}

variable "k8s_subnet_cidr" {
  type        = string
  description = "Kubernetes vms CIDR"
}

# variable "mgmt_public_ip" {
#   type        = string
#   description = "Management station public ip"
# }

variable "k8s_ebs_snapshot_id" {
  type        = string
  description = "Snapshot id of AMI's EBS"
}
