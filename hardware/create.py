# -*- coding: utf-8 -*-
"""
    component.create
    ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    
    Function to create a component
    
    :copyright: Spirent Commynications 2021, see AUTHORS for more details.
    :license: ISC, see LICENSE for more details.
"""

import os
import json
import shutil
import time
from common import read_config, read_resources, write_resources
from common import write_error, write_info
from common import get_map_value, set_map_value
from common import run_process, read_process
from common import save_terraform_state
from common import is_windows
from hardware import env_keypair_terraform, init_keypair_terraform, env_k8s_terraform, init_k8s_terraform, env_k8s_lb_terraform, init_k8s_lb_terraform, env_mgmt_terraform, init_mgmt_terraform


def create(config_path, resources_path):
    # Read config and resources
    config = read_config(config_path)
    resources = read_resources(resources_path)

    environment_prefix = get_map_value(config, "environment.prefix")
    path = os.path.abspath(os.path.dirname(__file__))


    # ### Keypair ###


    # Set terraform env variables and init terraform folder
    tf_vars = env_keypair_terraform(config, resources)
    keypair_terraform_path = init_keypair_terraform(environment_prefix)

    # Run terraform scripts
    os.chdir(keypair_terraform_path)
    exit_code = run_process("terraform init")
    if exit_code != 0:
        os.chdir(path + "/..")
        write_error("hw_keypair", "Can't initialize terraform. Watch logs above or check " + keypair_terraform_path +" folder content.")

    exit_code = run_process("terraform plan " + tf_vars)
    if exit_code != 0:
        os.chdir(path + "/..")
        write_error("hw_keypair", "Can't execute terraform plan. Watch logs above or check " + keypair_terraform_path +" folder content.")
    
    exit_code = run_process("terraform apply -auto-approve " + tf_vars)
    if exit_code != 0:
        os.chdir(path + "/..")
        write_error("hw_keypair", "Can't create cloud resources. Watch logs above or check " + keypair_terraform_path +" folder content.")

    # Get terraform outputs
    outputs = json.loads(str(read_process("terraform output -json")))
    keypair_name = outputs["keypair_name"]["value"]

    os.chdir(path + "/..")

    save_terraform_state("hw_keypair", environment_prefix)

    set_map_value(resources, "hw.k8s.keypair_name", keypair_name)
    set_map_value(resources, "hw.mgmt.keypair_name", keypair_name)
    write_resources(resources_path, resources)

    if not os.path.exists(f"{path}/../config/{keypair_name}.pem"):
        shutil.copy(f"{keypair_terraform_path}/secrets/{keypair_name}.pem", f"{path}/../config")


    # ### Kubernetes virtual machines ###


    # Set terraform env variables and init terraform folder
    tf_vars = env_k8s_terraform(config, resources)
    k8s_terraform_path = init_k8s_terraform(config, environment_prefix)

    # Run terraform scripts
    os.chdir(k8s_terraform_path)
    exit_code = run_process("terraform init")
    if exit_code != 0:
        os.chdir(path + "/..")
        write_error("kubernetes", "Can't initialize terraform. Watch logs above or check " + k8s_terraform_path +" folder content.")

    exit_code = run_process("terraform plan " + tf_vars)
    if exit_code != 0:
        os.chdir(path + "/..")
        write_error("kubernetes", "Can't execute terraform plan. Watch logs above or check " + k8s_terraform_path +" folder content.")
    
    exit_code = run_process("terraform apply -auto-approve " + tf_vars)
    if exit_code != 0:
        os.chdir(path + "/..")
        write_error("kubernetes", "Can't create cloud resources. Watch logs above or check " + k8s_terraform_path +" folder content.")

    # Get terraform outputs
    k8s_vm_count = int(get_map_value(config, "hw.k8s.nodes_count")) + int(get_map_value(config, "hw.k8s.masters_count"))
    os.environ["AWS_DEFAULT_REGION"] = get_map_value(config, "hw.aws.region")
    outputs = json.loads(str(read_process("terraform output -json")))
    for i in range(k8s_vm_count):
        k8s_sg_id = outputs[f"k8s_{i}_sg_id"]["value"]
        k8s_public_ip = outputs[f"k8s_{i}_public_ip"]["value"]

        set_map_value(resources, f"hw.k8s.{i}_id", outputs[f"k8s_{i}_id"]["value"])
        set_map_value(resources, f"hw.k8s.{i}_private_ip", outputs[f"k8s_{i}_private_ip"]["value"])
        set_map_value(resources, f"hw.k8s.{i}_public_ip", k8s_public_ip)
        set_map_value(resources, f"hw.k8s.{i}_sg_id", k8s_sg_id)

        # Open self public ip access
        run_process("aws ec2 authorize-security-group-ingress --group-id "+ k8s_sg_id + \
            " --protocol tcp --port 0-65535 --cidr " + k8s_public_ip + "/32")

    os.chdir(path + "/..")

    save_terraform_state("k8s", environment_prefix)

    # Save resources
    write_resources(resources_path, resources)

    # Save k8s nodes ip to config file
    master_ips = []
    worker_ips = []
    set_map_value(config, "k8s.username", get_map_value(config, "hw.k8s.username"))
    set_map_value(config, "k8s.private_key_path", f"config/{keypair_name}.pem")

    for i in range(get_map_value(config, "hw.k8s.masters_count")):
        master_ips.append(get_map_value(resources, f"hw.k8s.{i}_private_ip"))

    for i in range(get_map_value(config, "hw.k8s.masters_count"), k8s_vm_count):
        worker_ips.append(get_map_value(resources, f"hw.k8s.{i}_private_ip"))

    set_map_value(config, "k8s.master_ips", master_ips)
    set_map_value(config, "k8s.worker_ips", worker_ips)
    write_resources(config_path, config)


    # ### Kubernetes Load balancer virtual machine ###


    if get_map_value(config, "hw.k8s.masters_count") > 1:
        # Set terraform env variables and init terraform folder
        tf_vars = env_k8s_lb_terraform(config, resources)
        k8s_lb_terraform_path = init_k8s_lb_terraform(environment_prefix)

        # Run terraform scripts
        os.chdir(k8s_lb_terraform_path)
        exit_code = run_process("terraform init")
        if exit_code != 0:
            os.chdir(path + "/..")
            write_error("kubernetes_lb", "Can't initialize terraform. Watch logs above or check " + k8s_lb_terraform_path +" folder content.")

        exit_code = run_process("terraform plan " + tf_vars)
        if exit_code != 0:
            os.chdir(path + "/..")
            write_error("kubernetes_lb", "Can't execute terraform plan. Watch logs above or check " + k8s_lb_terraform_path +" folder content.")
        
        exit_code = run_process("terraform apply -auto-approve " + tf_vars)
        if exit_code != 0:
            os.chdir(path + "/..")
            write_error("kubernetes_lb", "Can't create cloud resources. Watch logs above or check " + k8s_lb_terraform_path +" folder content.")

        # Get terraform outputs
        outputs = json.loads(str(read_process("terraform output -json")))
        k8s_lb_id = outputs["k8s_lb_id"]["value"]
        k8s_lb_private_ip = outputs["k8s_lb_private_ip"]["value"]
        k8s_lb_public_ip = outputs["k8s_lb_public_ip"]["value"]
        k8s_lb_sg_id = outputs["k8s_lb_sg_id"]["value"]

        os.chdir(path + "/..")

        save_terraform_state("k8s_lb", environment_prefix)

        # Save resources
        set_map_value(resources, "hw.k8s.lb_id", k8s_lb_id)
        set_map_value(resources, "hw.k8s.lb_private_ip", k8s_lb_private_ip)
        set_map_value(resources, "hw.k8s.lb_public_ip", k8s_lb_public_ip)
        set_map_value(resources, "hw.k8s.lb_sg_id", k8s_lb_sg_id)
        write_resources(resources_path, resources)

        set_map_value(config, "k8s.loadbalancer.host", k8s_lb_private_ip)
        write_resources(config_path, config)


    # ### Management Station ###


    mgmt_keypair_name = environment_prefix + "-mgmt-station"

    # Set terraform env variables and init terraform folder
    tf_vars = env_mgmt_terraform(config, resources)
    mgmt_terraform_path = init_mgmt_terraform(environment_prefix)

    # Run terraform scripts
    os.chdir(mgmt_terraform_path)
    exit_code = run_process("terraform init")
    if exit_code != 0:
        os.chdir(path + "/..")
        write_error("mgmt_station", "Can't initialize terraform. Watch logs above or check " + mgmt_terraform_path +" folder content.")

    exit_code = run_process("terraform plan " + tf_vars)
    if exit_code != 0:
        os.chdir(path + "/..")
        write_error("mgmt_station", "Can't execute terraform plan. Watch logs above or check " + mgmt_terraform_path +" folder content.")
    
    exit_code = run_process("terraform apply -auto-approve " + tf_vars)
    if exit_code != 0:
        os.chdir(path + "/..")
        write_error("mgmt_station", "Can't create cloud resources. Watch logs above or check " + mgmt_terraform_path +" folder content.")

    # Get terraform outputs
    outputs = json.loads(str(read_process("terraform output -json")))
    mgmt_private_ip = outputs["private_ip"]["value"][0][0]
    mgmt_public_ip = outputs["public_ip"]["value"][0][0]

    os.chdir(path + "/..")

    save_terraform_state("mgmt_station", environment_prefix)

    set_map_value(resources, "hw.mgmt_station.key_name", mgmt_keypair_name)
    set_map_value(resources, "hw.mgmt_station.public_ip", mgmt_public_ip)
    set_map_value(resources, "hw.mgmt_station.private_ip", mgmt_private_ip)
    write_resources(resources_path, resources)

    write_info ("mgmt_station", "Waiting 2 minutes while mgmt station is initializing...")
    time.sleep(120)

    if get_map_value(config, "hw.mgmt_station.publicly_available") == True: 
        mgmt_ip = mgmt_public_ip
    else:
        mgmt_ip = mgmt_private_ip

    # Copy whole project to mgmt station
    if get_map_value(config, "hw.mgmt_station.copy_project_to_mgmt_station") == True: 
        mgmt_instance_username = get_map_value(config, "hw.mgmt_station.instance_username")

        run_process("ssh -o StrictHostKeyChecking=accept-new " + mgmt_instance_username + "@" + mgmt_ip + \
            " -i " + mgmt_terraform_path + "/secrets/" + mgmt_keypair_name + \
            ".pem \"mkdir -p /home/"+ mgmt_instance_username + "/env-onprem2-python\"" 
        )
        print("Copying files from local to mgmt station...")

        if is_windows():
            run_process ("scp -i " + mgmt_terraform_path + "/secrets/" + mgmt_keypair_name + ".pem" + \
                " -r " + path + "/../* " + mgmt_instance_username + "@" + mgmt_ip + ":/home/"+ mgmt_instance_username + "/env-onprem2-python"
            )
        else:
            # linux or macos
            run_process ("scp -i " + mgmt_terraform_path + "/secrets/" + mgmt_keypair_name + ".pem" + \
                " -r " + path + "/.. " + mgmt_instance_username + "@" + mgmt_ip + ":/home/"+ mgmt_instance_username
            )
        write_info ("mgmt_station", "Copying files from local to mgmt station is done")

        # Save ssh commands
        ssh_command = "ssh -i " + mgmt_terraform_path + "/secrets/" + mgmt_keypair_name + ".pem " + \
            mgmt_instance_username + "@" + mgmt_ip
        set_map_value(resources, "hw.mgmt_station.ssh_command", ssh_command)
        write_resources(resources_path, resources)

        write_info ("mgmt_station", "Mgmt station ssh command: " + ssh_command, False, "Green")

    else:
        write_info("mgmt_station", "Copying project to mgmt station skipped because it is disabled in config file.")
