# -*- coding: utf-8 -*-
"""
    common.templates
    ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    
    Functions to process template files
    
    :copyright: Spirent Commynications 2021, see AUTHORS for more details.
    :license: ISC, see LICENSE for more details.
"""

import re
import base64

def convert_from_template(template, params1 = dict(), params2 = dict(), base64enc = False):
    params = dict()
    for key in params1:
        params[key] = params1[key]
    for key in params2:
        params[key] = params2[key]

    begin_tag = "<%="
    end_tag = "%>"
    output = ""

    template = template.replace("\r\n", "\r")
    template = template.replace("\n", "\r")
    regex = re.compile("(?P<pre>.*?)<%=(?P<key>.*?)%>(?P<post>.*)")

    while (matches := regex.match(template)) != None:
        template = matches.group("post")
        key = matches.group("key").strip()
        value = params[key] + ""
        if base64enc:
            value = base64.b64encode(value.encode("ascii"))
        output += matches.group("pre") + value

    output += template
    output = output.replace("\r", "\n")
    return output

def build_template(input_path, output_path, params1 = dict(), params2 = dict(), base64enc = False):
    with open(input_path, 'r', encoding = 'utf-8') as f:
        template = f.read()

    if template != None:
        value = convert_from_template(template, params1, params2, base64enc)
    else:
        value = ""

    with open(output_path, 'w', encoding = 'utf-8') as f:
        f.write(value) 