# Install powershell
brew install powershell

# Install AWS cli
brew install awscli

# Install ansible
brew install ansible

# Install kubectl
brew install kubectl

# Install helm
brew install helm
