# <img src="https://upload.wikimedia.org/wikipedia/en/thumb/c/ce/Spirent_logo.svg/490px-Spirent_logo.svg.png" alt="Spirent Logo" width="200"> <br/> Spirent VisionWorks On-Premises Environment

This is the environment automation scripts to create, update and delete On-premises Environments. The On-premises environments are functionally 
identical to cloud environments, but are run locally on the customer's hardware.
The on-premises environments are  deployed by a customer on their own infrastructure. This environment can be connected to one or more field installations that belong to that customer.

The environment consists of the following components:
* **Kubernetes** cluster
* **Namespace** infra namespace for infrastructure services
* **Environment** config map with information about the environment

In the 2nd major update of the environment all infrastructure were moved out of the environment and placed into the [package-infraservices-helm](https://git.vwx.spirent.com/vwx-delivery/package-infraservices-helm) component

<img src="design.png" width="800">

<a name="links"></a> Quick links:

* [Delivery Platform Requirements](https://frkengjira01.spirentcom.com/confluence/display/VWX/Delivery+Platform+Requirements)
* [Delivery Platform Architecture](https://frkengjira01.spirentcom.com/confluence/display/VWX/Delivery+Platform+Architecture)
* [Change Log](CHANGELOG.md)


# Project structure
| Folder | Description |
|----|----|
| common | Scripts with support functions like working with configs, templates etc. | 
| config | Config files for scripts. Also stores *resources* files, created automaticaly. | 
| hardware | Contains TerraForm scripts for creating resources in AWS, simulating the customer's on-premises hardware, to test and/or demo the environemnt. |


## Use

### Setting up hardware simulation

Go to the `config` folder and create a configuration for the new environment.
Read the configuration section below and use [sample_config.json](config/sample_config.json)
as your starting point.

Your configuration may look like the sample below.
```json
{
    "env": {
      "type":  "onprem",
      "version":  "1.0.0",
    },

    "k8s": {
      "namespace":  "infra",
      "network":  "10.0.0.0/16",
      "cni_version":  "0.6.0-00",
      "kubelet_version":  "1.20.2-00",
      "kubeadm_version":  "1.20.2-00",
      "kubectl_version":  "1.20.2-00",
      "crictl_version":  "v1.14.0",
    }
}
```

In the same `config` folder, create a configuration for the HW simulation.
Your HW configuration may look like the sample below.
```json
{
    "env": {
      "type": "onprem",
      "version": "1.0.0",
      "name": "spirent-onprem-demo",
    },

    "hw": {
      "cloud": "aws",

      "aws": {
        "access_id": "XXX",
        "access_key": "XXX",
        "region": "us-east-1",
        "vpc": "XXX"
      },
      "mgmtstation": {
        "subnet_zone": "us-east-1a",
        "subnet_cidr": "10.0.0.0/28",
        "ssh_allowed_cidr_blocks": [
            "127.0.0.1/32"
        ],
        "username": "ubuntu",
        "keypair_name": "id_rsa",
        "instance_type": "t2.medium",
        "instance_ami": "ami-43a15f3e",
        "copy_project_to_mgmt_station": true,
        "create": true
      },
      "k8s": {
        "subnet_zone": "us-east-1a",
        "subnet_cidr": "10.0.0.16/28",
        "username": "ubuntu",
        "keypair_name": "id_rsa",
        "instance_type": "t2.medium",
        "instance_ami": "ami-43a15f3e",
        "nodes_count": 2
      }
    }
}
```

Create the HW resources needed by the environment. This includes a management station, that will be used to create new environments, instances for the kubernetes master and worker nodes, and an instance for the Timescale database.
```bash
python .\create_hw.py --config=<path to config file>
```

After you run the script you shall see a HW resource file (ending with `resources-hw.json`) next to the config file you created.

Connect to the newly created management station (see it's address and credentials in the script output).

### Creating the environment

**The following steps shall be performed from the management station**.

Note: if using a HW simulation, the first 2 steps of getting and unzipping the project are done automatically by the create-hw scripts.

Start the environment creation process from the management station. Download released package with environment provisioning scripts from http://artifactory.vwx.spirent.com/misc-binaries/vwx-delivery/env-onprem-python-1.0.0.zip
```bash
wget http://artifactory.vwx.spirent.com/misc-binaries/vwx-delivery/env-onprem-python-1.0.0.zip
```

Unzip scripts from the package
```bash
unzip env-onprem-python-1.0.0.zip
```

Install prerequisites on the management station by running the installation script for the corresponding platform.
```bash
install_prereq_[win | mac | ubuntu].[ps1 | sh]
```

If the environment is being run using HW simulation, copy the files in the "config" folder to the management station's "config" folder. Otherwise, create an environment config file (see the example listed above) that contains information about the on-premises infrastructure.

Create a new environment. The resources file will contain references to newly created resources.
```bash
python create_env.py --config=<path to config file>
```

When environment configuration changes, you can update it without loosing data or reinstalling applications.
```bash
python update_env.py --config=<path to config file>
```

Delete the environment when its no longer needed to free up used computing resources.
```bash
python delete_env.py --config=<path to config file>
```

## Configuration

The environment configuration supports the following configuration parameters:

| Parameter | Default value | Description |
|----|----|---|
| type | onprem | Type of environment |

Example environment config file:
```json
{
  "environment": {
    "type": "onprem",
    "version": "1.0.0",
    "name": "spirent-onprem-test"
  },
  "extended_ansible_logs": true,
  "k8s": {
    "namespace": "infra",
    "network": "10.0.0.0/16",
    "cni_version": "0.6.0-00",
    "kubelet_version": "1.20.2-00",
    "kubeadm_version": "1.20.2-00",
    "kubectl_version": "1.20.2-00",
    "crictl_version": "v1.14.0",
    "master_ips": [
      "172.31.2.153"
    ],
    "worker_ips": [
      "172.31.2.108"
    ],
    "loadbalancer":  {
      "host":  "spirent-onprem-demo-k8s-lb-518727028.us-east-1.elb.amazonaws.com",
      "port":  "6443"
    },
    "username": "ubuntu",
    "private_key_path": "temp/keypair/secrets/sbx006-sandbox-spirent-onprem-test.pem"
  }
}
```


## Resources

As the result of the scripts information about created resources will be saved into a resource file that is ended with `resources.json`
and placed next to the configuration file used to create an environment.

Resource parameters list:
| Parameter | Ex. value | Description |
|----|----|---|
|k8s.address| xxx.xxx.xxx.xxx | IP address of the k8s node |
<!-- Todo -->

Example environment resources file:
```json
{
  "environment": {
    "create_time": "2021-03-24T19:12:43.7894904+00:00",
    "version": "1.0.0",
    "type": "onprem"
  },
  "connectparams": {
    "status": "created"
  },
  "k8s": {
    "namespace": "infra",
    "type": "kubeadm",
    "inventory": [
      "[master_init]",
      "master ansible_host=172.31.2.153 ansible_ssh_user=ubuntu ansible_ssh_private_key_file=temp/keypair/secrets/sbx006-sandbox-spirent-onprem-test.pem",
      "\r\n[masters_redundant]",
      "\r\n[workers]",
      "worker0 ansible_host=172.31.2.108 ansible_ssh_user=ubuntu ansible_ssh_private_key_file=temp/keypair/secrets/sbx006-sandbox-spirent-onprem-test.pem"
    ]
  }
}
```


## Known issues

(None)


## Contacts

This environment was created and currently maintained by the team managed by *Sergey Seroukhov* and *Raminder Singh*.
